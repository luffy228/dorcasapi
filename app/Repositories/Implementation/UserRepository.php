<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\User;

class UserRepository extends GenericRepository
{

    public function model()
    {
        return 'App\User';
    }

}
