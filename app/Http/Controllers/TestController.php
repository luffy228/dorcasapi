<?php

namespace App\Http\Controllers;


use App\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;




class TestController extends Controller
{
    protected $userRepo;
     function __construct(App $app)
    {
        $this->userRepo =new UserRepository($app);
    }

    public function index(){

        dd($this->userRepo->getModel());
    }

}
